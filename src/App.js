import React, { Component } from 'react';
import { Player } from './components/player.component';
import { Stadium } from './components/stadium.component';
import { ProgressBar } from './components/progress.component';
import { PlayButton } from './components/playButton.component';
import { Timer } from './components/timer.component';
import { data } from './fixtures/data';

export const PLAYERS_COUNT = 22;

class App extends Component {
  state = {
    time: 0,
    progress: 0,
    started: false,
    isStarted: true,
    requestId: null,
    offset: 0,
    interval: data.interval,
  };

  renderInitialPositions = () => {
    let initialData = [];
    for(let i = 1; i < PLAYERS_COUNT; i++) {
      initialData.push(<Player playerNumber={i} key={i} />)
    }
    return initialData;
  };

  handlePlaybackEvent = e => {
    this.goTo(e.nativeEvent.offsetX / e.target.offsetWidth);
  };

  handlePause = isStarted => {
    window.cancelAnimationFrame(this.state.requestId);
    this.setState({ isStarted }, () => {
      if (this.state.isStarted) {
        this.setState({ requestId: this.loop(this.state.time) });
      }
    });
  };

  intializeApp = () => {
    this.progressBar = document.querySelectorAll('.progress')[0];
    this.stadium = document.querySelectorAll('.stadium')[0];
    this.stadiumWidth = this.stadium.offsetWidth;
    this.stadiumHeight = this.stadium.offsetHeight;
    this.setState({ requestId: window.requestAnimationFrame(this.loop) });
  };

  goTo = progress => {
    this.setState({ started: null });
    this.setState({ offset: progress });
    window.cancelAnimationFrame(this.state.requestId);
    this.setState({ requestId: window.requestAnimationFrame(this.loop) });;
  };
  
  loop = timestamp => {
    if (!this.state.isStarted) {
      return;
    }
    let frame = Math.ceil((timestamp - this.state.started) / this.state.interval);
    let frameWithOffset = frame + Math.ceil(this.state.offset * data.player_positions.length);
    let frameData = data.player_positions[frameWithOffset];
    if (!this.state.started) {
      this.setState({ started: timestamp });
    }
    if (!frameData) {
      return;
    }
    frameData.forEach(item => {
      this.updatePlayerPosition(item[0], item[1], item[2]);
    });
    this.updateProgressBar(
      frameWithOffset * this.state.interval,
      frame / data.player_positions.length + this.state.offset
    );
    this.setState({ requestId: window.requestAnimationFrame(this.loop) });
  };

  updatePlayerPosition = (id, x, y) => {
    const player = document.getElementById(`#player${id}`);
    player.style.left = `${x * this.stadiumWidth}px`;
    player.style.bottom = `${y * this.stadiumHeight}px`;
  };

  updateProgressBar = (time, progress) => {
    this.setState({
      time,
      progress: progress * 100,
    });
  };

  componentDidMount() {
    this.intializeApp();
  }

  render() {
    const { time, progress} = this.state;
    return (
      <div className="app">
        <div className="app__wrapper">
          <div>
            <Timer time={time} />
            <PlayButton onPause={this.handlePause} />
          </div>
          <Stadium>
            {this.renderInitialPositions()}
          </Stadium>
          <ProgressBar
            onClick={this.handlePlaybackEvent}
            completed={progress}
          />
        </div>
      </div>
    );
  }
}

export default App;
