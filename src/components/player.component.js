import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export class Player extends PureComponent {
  static propTypes = {
    playerNumber: PropTypes.number,
  };

  render() {
    const {
      playerNumber,
    } = this.props;
    return (
      <div
        id={`#player${playerNumber}`}
        className="player"
      >
        {playerNumber}
      </div>
    )
  }
}

