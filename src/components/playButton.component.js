import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export class PlayButton extends PureComponent {
  static propTypes = {
    onPause: PropTypes.func.isRequired,
  };

  state = {
    isStarted: true,
  };

  onClick = () => {
    this.setState({ isStarted: !this.state.isStarted}, () => {
      this.props.onPause(this.state.isStarted);
    });
  };

  renderCaption = () => this.state.isStarted ? 'PLAY' : 'PAUSE';

  render() {
    return (
      <div
        className={'play-button'}
        onClick={this.onClick}
      >
        {this.renderCaption()}
      </div>
    )
  }
}

