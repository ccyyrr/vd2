import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export class Timer extends PureComponent {
  static propTypes = {
    time: PropTypes.number,
  };

  formatTime = time => {
    const miliseconds = time % 1000;
    const seconds = ((time - miliseconds) / 1000) % 60;
    const minutes = parseInt((time / 1000) - seconds, 10) / 60;
    return `${minutes}:${seconds}`
  };

  render() {
    return (
      <div className="timer">
        <div>
          {'TIMER'}
        </div>
        {this.formatTime(this.props.time)}
      </div>
    );
  }
}

