import React, { PureComponent } from 'react';
import Progress  from 'react-progressbar';
import PropTypes from 'prop-types';

export class ProgressBar extends PureComponent {
  static propTypes = {
    completed: PropTypes.number,
    onClick: PropTypes.func,
  };

  render() {
    const { completed, onClick } = this.props;
    return (
      <div
        onClick={onClick}
        className="progress"
      >
        <Progress 
          completed={completed}
        />
      </div>
    );
  }
}

