import React, { PureComponent } from 'react';
export class Stadium extends PureComponent {
  render() {
    return (
      <div className="stadium">
        {this.props.children}
      </div>
    );
  }
}

